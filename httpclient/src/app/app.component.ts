import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'httpclient';
  todo: any;

  constructor(private http: HttpClient) {}

  Event() {
    this.http
      .get('https://http-d6945-default-rtdb.firebaseio.com/data.json')
      .subscribe((response: any) => {
        // console.table(data);
        this.todo = response;
      });
  }
  onCreatePost(postData: { title: string; password: string }) {
    this.http
      .post(
        'https://http-d6945-default-rtdb.firebaseio.com/data.json',
        postData
      )
      .subscribe((response) => {
        console.log(response);
      });
  }
  delete() {
    this.http
      .delete('https://http-d6945-default-rtdb.firebaseio.com/data.json')
      .subscribe((response) => {
        console.log(response);
      });
  }
}
