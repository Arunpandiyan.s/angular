import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {
  //todo: any;
  constructor(private http: HttpClient) {}
  get(url: string) {
    return this.http.get(url);
  }
}
