import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'input';
  userObject = {
    name: 'arun',
    age: '21',
    id: 123,
  };
  userhandle(event: any) {
    console.log(event);
  }
}
