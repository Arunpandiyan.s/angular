import { Component, Input, Output, EventEmitter } from '@angular/core';
interface userInterface {
  name: string;
  age: string;
  id: number;
}
@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
})
export class ChildComponent {
  @Input() user: userInterface;
  @Output() userEvent = new EventEmitter<userInterface>();

  constructor() {
    this.user = {} as userInterface;
  }
  senduserEvent(): void {
    this.userEvent.emit(this.user);
  }
}
