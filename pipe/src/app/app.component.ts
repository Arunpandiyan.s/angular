import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'pipe';
  pipe = '';
  ab = { name: 'arun', age: 21 };
  thisDate = new Date();
  price = 10050.4521;
  a: number = 10;
}
