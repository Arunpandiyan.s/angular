import { Component } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'observable';
  constructor() {
    const test$ = new Observable((Subscriber) => {
      console.log('test');
      Subscriber.next('1');
      Subscriber.next('2');
      Subscriber.error('error occured');
      Subscriber.next('3');
      //setTimeout(() => Subscriber.next('4'), 1000);
    });
    test$.subscribe(
      (x) => {
        console.log(x);
      },
      (error) => {
        console.log(error);
      }
      // (complete) => {
      //   console.log('code commit');
      // }
    );
    // test$.subscribe((y) => {
    //   console.log(y);
    // });
  }
}
